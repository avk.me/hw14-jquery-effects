const up = document.createElement('p');

$(window).scroll(function () {
    if ($(window).scrollTop() > document.documentElement.clientHeight) {
        $(up).addClass("up");
        up.innerHTML = "НАВЕРХ";
        document.body.prepend(up);
        up.addEventListener('click', function () {
            window.scrollTo(0, 0);
        })

    } else {
        if (document.querySelector(".up") !== "null") {
            document.querySelector('.up').remove();
        }
    }
});

$(document).on("click", ".toggle-btn", function () {
    $('.posts').slideToggle(2000);
});